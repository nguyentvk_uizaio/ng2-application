import {Injectable} from '@angular/core';
import {Http, Response, RequestOptions} from '@angular/http';
import {HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ErrorObservable} from 'rxjs/observable/ErrorObservable';
import {catchError, map, tap, retry} from 'rxjs/operators';
import {tryCatch} from 'rxjs/util/tryCatch';
import 'rxjs/add/operator/map';
import {ConfigService} from './ConfigService.service';
import {tryStatement} from 'babel-types';

@Injectable()
export class ApiService {
  constructor(private _http: Http) {
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('A client-side or network error occurred:', error);
      return new ErrorObservable(error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(error);
      console.error(`Backend returned code ${error.status}`, `body was: ${error.error}`);
      return new ErrorObservable(error);
    }
    // return new ErrorObservable( 'Something bad happened; please try again later.');
  }

  private request(options: any = {}): Observable<any[]> {
    let url = `${ConfigService.apiUrl}${options.url}`;
    options.method = options.method || 'get';
    options.headers = options.headers || {'Content-Type': 'application/json', 'Cache-Control': 'no-cache'};
    if (options && options.method && options.method.toUpperCase() === 'GET') {
      let requestOptions = new RequestOptions({headers: options.headers, search: (options.data || {})});
      return this._http.get(url, requestOptions).map((res: Response) => res.json());
    }
    if (options && options.method && options.method.toUpperCase() === 'POST') {
      //create
      return this._http.post(url, (options.data || {}), options.headers).map((res: Response) => res.json());
    }
    if (options && options.method && options.method.toUpperCase() === 'PUT') {
      //update url = url/:id
      return this._http.put(url, (options.data || {}), options.headers).map((res: Response) => res.json());
    }
    if (options && options.method && options.method.toUpperCase() === 'DELETE') {
      //delete url = url/:id
      let optionsDelete = new RequestOptions({headers: options.headers, body: (options.data || {})});
      return this._http.delete(url, optionsDelete).map((res: Response) => res.json())
        .pipe(
          retry(3),
          catchError(this.handleError)
        );
    }
  }

  public getListPost(params: any = {}): Observable<any[]> {
    let option = {
      url: '/v1/post',
      method: 'get',
      data: params
    };
    return this.request(option);
  }

  public getPostDetail(params: any = {}): Observable<any[]> {
    let option = {
      url: '/v1/post/' + params.id,
      method: 'get',
      data: params
    };
    return this.request(option);
  }

  public updateHero(params: any = {}): Observable<any[]> {
    let option = {
      url: '/v1/post/' + params.id,
      method: 'put',
      data: params
    };
    return this.request(option);
  }

  public deleteHero(params: any = {}): Observable<any[]> {
    let option = {
      // url: '/v1/post/' + params.id,
      url: '/v1/post/',
      method: 'delete',
      data: params
    };
    return this.request(option);
  }
}
