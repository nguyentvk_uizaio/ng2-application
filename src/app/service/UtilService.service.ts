import {Injectable} from '@angular/core';
import {ToasterModule, ToasterService, ToasterConfig} from 'angular2-toaster';

@Injectable()
export class UtilService {
  constructor(private _toasterService: ToasterService) {
    this._toasterService = _toasterService;
  }
  notify(title = '', message = '', type='success'){
    this._toasterService.pop(type,title, message);
  }
  notifySuccess(title = '', message = ''){
    this._toasterService.pop('success' ,title, message);
  }
  notifyError(title = '', message = ''){
    this._toasterService.pop('error' ,title, message);
  }
}
