import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, PLATFORM_ID, APP_ID, Inject, OnInit, OnDestroy } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { isPlatformBrowser, isPlatformServer, CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ToasterModule, ToasterService, ToasterContainerComponent } from 'angular2-toaster'; //notifycation
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { TooltipModule } from 'ngx-tooltip'; //tooltip
import { ModalModule, AlertModule, BsDatepickerModule } from 'ngx-bootstrap';//https://valor-software.com/ngx-bootstrap
import * as _ from 'lodash';
import * as async from 'async';

//route
import { appRoutes } from './app.route';

//component
import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { HeroesComponent } from './component/heroes/heroes.component';
import { HeroDetailComponent } from './component/hero-detail/hero-detail.component';
import { HeroDetailOverviewComponent } from './component/hero-detail/overview/overview.component';
import { HeroDetailSkillComponent } from './component/hero-detail/skill/skill.component';
import { PageNotFoundComponent } from './directive/pageNotFound/pageNotFound.component';

//directive
import { FormdataComponent } from './directive/formdata/formdata.component';
import { UizaDatatableComponent } from './directive/datatable/datatable.component';

//pipe filter
import { MyFilter } from './filter/myFilter.pipe';

//service
import { ApiService } from './service/ApiService.service';
import { UtilService } from './service/UtilService.service';
import { ConfigService } from './service/ConfigService.service';
import { HighlightDirective } from './directive/highlight/highlight.directive';
import { LoginComponent } from './component/login/login.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable'
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeroesComponent, HeroDetailComponent, HeroDetailOverviewComponent, HeroDetailSkillComponent,
    PageNotFoundComponent,
    FormdataComponent, UizaDatatableComponent, HighlightDirective,

    MyFilter,

    LoginComponent


  ],
  imports: [
    BrowserModule,
    HttpModule, HttpClientModule,
    ChartsModule,
    BrowserAnimationsModule,
    ToasterModule.forRoot(),
    FormsModule,
    CommonModule,
    TooltipModule,
    AlertModule.forRoot(), BsDatepickerModule.forRoot(), ModalModule.forRoot(),
    appRoutes,
    NgxDatatableModule
  ],
  providers: [
    ConfigService,
    ApiService,
    UtilService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AppModule {
}
