import { Component, OnInit, Input, Output, EventEmitter, ViewChild, NgZone } from '@angular/core';
import { ApiService } from '../../service/ApiService.service';
import { UtilService } from '../../service/UtilService.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { DATA } from './mockUpData'
import * as _ from 'lodash'
@Component({
  selector: 'uiza-data-table',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.scss'],
  providers: [ApiService, UtilService]
})

export class UizaDatatableComponent implements OnInit {
  @ViewChild(DatatableComponent) dataTable: DatatableComponent;

  @Input() tableData: any = DATA.TABLE_DATA;
  @Input() tableOptions: any = {
    limits: [5, 10, 20, 50, 100],
    totalItems: 20,
    currentPage: 1,
    limit: 10,
    queryWithParam: null,
    isCheckedAll: false,
    form: [
      {
        name: 'Name',
        key: 'name',
        type: ''
      }, {
        name: 'Company',
        key: 'company',
        type: ''
      }, {
        name: 'Gender',
        key: 'gender',
        type: ''
      }, {
        name: 'Age',
        key: 'age',
        type: ''
      }
    ],
    orderBy: '',
    searchBy: 'title',
    asc: true,
    add: 'urlLinkAdd',
    edit: (item) => {
      console.log('edit ', item);
    },
    delete: (item) => {
      console.log('delete ', item);
    },
    view: (item) => {
      console.log('view', item);
    }
  };

  selectedItems = [];


  constructor(private _apiService: ApiService, private _utilService: UtilService, private zone: NgZone) {
  }

  ngOnInit() {
  }

  /**
   * Example for filter/search features
   * @param event 
   */
  temp = _.cloneDeep(this.tableData);  // save origin data
  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function (d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.tableData = temp;
    // Whenever the filter changes, always go back to the first page
    this.dataTable.offset = 0;
  }

  /**
   * when limit change / calculate paging
   */
  _limit = this.tableOptions.limit;
  changeLimit = () => {
    // bug fixing - ngx-datatable contribute - limit bug when change page
    while(this._limit != this.tableOptions.limit) {
      if(this._limit < this.tableOptions.limit) {
        this._limit++;
      } else {
        this._limit--;
      }
    }
    this.tableData = [...this.tableData]
    this.dataTable.offset = 0;
    this.dataTable.limit = this._limit ;
  }

  /**
   * event when change page
   */
  onPaginated = (event) => {
    this.dataTable.recalculate();
  }
  
  onSelect({ selected }) {
    this.selectedItems.splice(0, this.selectedItems.length);
    this.selectedItems.push(...selected);
  }

  onActivate(event) {
    // console.log('Activate Event', event);
  }

  displayCheck(row) {
    return row.name !== 'Ethel Price';
  }

}
