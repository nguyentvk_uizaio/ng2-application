import { Component, OnInit} from '@angular/core';
import { ApiService } from '../../service/ApiService.service';

@Component({
  selector: 'home-component',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [ApiService]
})
export class HomeComponent implements OnInit {
  constructor(private apiService: ApiService) { }
  ngOnInit() {
  }
}
