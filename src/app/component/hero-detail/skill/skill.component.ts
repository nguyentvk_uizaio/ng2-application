import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../service/ApiService.service';

@Component({
  selector: 'hero-detail-skill',
  templateUrl: './skill.component.html',
  styleUrls: ['./skill.component.scss']
})
export class HeroDetailSkillComponent implements OnInit, OnChanges, OnDestroy {
  private parentRouteId: number;
  constructor(private apiService : ApiService, private _activedRoute: ActivatedRoute) {
  }
  ngOnInit() {
    this._activedRoute.parent.params.subscribe(params => {
      this.parentRouteId = params["id"];
      console.log('this.parentRouteId',this.parentRouteId);
    });
  }
  ngOnChanges(){}
  ngOnDestroy(){}
}
